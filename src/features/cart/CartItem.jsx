import DeleteItem from "./DeleteItem";
import UpdateCartQuantity from "./UpdateCartQuantity";

function CartItem({ item }) {
  const { pizzaId, name, quantity } = item;
  return (
    <li className=" flex justify-between items-center py-3">
      <p className=" mb-1">
        {quantity}&times; {name}
      </p>
      <div className=" flex items-center space-x-4 font-bold ">
        <UpdateCartQuantity id={pizzaId}/>
       <DeleteItem pizzaId={pizzaId}/>
      </div>
    </li>
  );
}

export default CartItem;
