import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { getTotalCartPrice, getTotalCartQty } from "./cartSlice";

function CartOverview() {
  const  quantity  = useSelector(getTotalCartQty);
  const total =  useSelector(getTotalCartPrice)

  if (quantity<1) {
    return null;
  }
  return (
    <div className=" bg-stone-800 uppercase tracking-widest text-white flex justify-between py-4 px-4">
      <p className=" space-x-4 font-semibold text-stone-300">
        <span>{quantity} pizzas</span>
        <span>${total}</span>
      </p>
      <Link to={"/cart"}>Open cart &rarr;</Link>
    </div>
  );
}

export default CartOverview;
